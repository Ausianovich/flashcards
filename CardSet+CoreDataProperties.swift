//
//  CardSet+CoreDataProperties.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//
//

import Foundation
import CoreData


extension CardSet {
    
    enum SortKeys: String {
        case title
    }

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CardSet> {
        return NSFetchRequest<CardSet>(entityName: String(describing: CardSet.self))
    }

    @NSManaged public var title: String?
    @NSManaged public var describing: String?

    @NSManaged public var cards: NSSet?

    func createCardSet(withTitle title: String, describing: String? = nil, cards: [Card] = []) {
        self.title = title
        self.describing = describing
        for item in cards {
            self.addToCards(item)
        }
    }
}

// MARK: Generated accessors for cards
extension CardSet {

    @objc(addCardsObject:)
    @NSManaged public func addToCards(_ value: Card)

    @objc(removeCardsObject:)
    @NSManaged public func removeFromCards(_ value: Card)

    @objc(addCards:)
    @NSManaged public func addToCards(_ values: NSSet)

    @objc(removeCards:)
    @NSManaged public func removeFromCards(_ values: NSSet)

}
