//
//  Card+CoreDataProperties.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//
//

import Foundation
import CoreData


extension Card {
    
    enum SortKeys: String {
        case term, definition, owner
    }

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Card> {
        return NSFetchRequest<Card>(entityName: String(describing: Card.self))
    }

    @NSManaged public var term: String?
    @NSManaged public var definition: String?
    @NSManaged public var cardDescription: String?
    @NSManaged public var owner: CardSet?

    func configureCard(withTerm term: String, definition: String, cardDescription: String?, owner: CardSet) {
        self.term = term
        self.definition = definition
        self.cardDescription = cardDescription
        self.owner = owner
    }
}
