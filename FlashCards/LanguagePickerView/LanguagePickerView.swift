//
//  LanguagePickerView.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 29.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit

final class LanguagePickerView: UIViewController {
    
    //MARK: - Typealias
    public typealias Values = [[String]]
    public typealias Index = (column: Int, row: Int)
    public typealias Action = (_ vc: UIViewController, _ picker: UIPickerView, _ index: Index, _ values: Values) -> ()
    
    //MARK: - Property
    private lazy var pickerView = UIPickerView()
    
    private var action: Action?
    private var initialSelection: Index?
    private var values: Values = [[]]

    //MARK: - Init
    init(withValues values: Values,
         initialSelection: Index? = nil,
         action: Action?) {
        super.init(nibName: nil, bundle: nil)
        self.values = values
        self.initialSelection = initialSelection
        self.action = action
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Main functions
    override func loadView() {
        view = pickerView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let initialSelection = self.initialSelection,
               values.count > initialSelection.column,
               values[initialSelection.column].count > initialSelection.row {
            self.pickerView.selectRow(initialSelection.row, inComponent: initialSelection.column, animated: true)
        }
    }
}

//MARK: - UIPickerViewDelegate
extension LanguagePickerView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return values[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        action?(self, pickerView, Index(column: component, row: row), self.values)
    }
}

//MARK: - UIPickerViewDataSource
extension LanguagePickerView: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.values.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.values[component].count
    }
}
