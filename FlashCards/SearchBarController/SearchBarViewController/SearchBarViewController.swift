//
//  SearchBarViewController.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit

final class SearchBarViewController: UIViewController {
    
    //MARK: - Property
    lazy var tapRecognizer: UITapGestureRecognizer = {
      var recognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
      return recognizer
    }()
    
    private var viewModel: SearchBarViewControllerViewModel!
    private var defaults = UserDefaults.standard
    
    //MARK: - @IBOutlets
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var termLanguageButton: UIButton!
    @IBOutlet var definitionLanguageButton: UIButton!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
        
        let cellNib = UINib(nibName: String(describing: SearchBarTableViewCell.self), bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: String(describing: SearchBarTableViewCell.self))
        
        self.viewModel = SearchBarViewControllerViewModel(delegate: self)
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        self.tableView.addGestureRecognizer(self.tapRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.termLanguageButton.setTitle(UserDefaults.standard.string(forKey: self.termLanguageButton.restorationIdentifier ?? ""), for: .normal)
        self.definitionLanguageButton.setTitle(UserDefaults.standard.string(forKey: self.definitionLanguageButton.restorationIdentifier ?? ""), for: .normal)
    }
    
    //MARK: - IBActions
    private func showLanguageAlertController(_ sender: UIButton, _ initialSelection: String) {
        let alertController = UIAlertController(title: "Set language", message: nil, preferredStyle: .actionSheet)
        
        let initialSelection: LanguagePickerView.Index = (0, Language.init(rawValue: initialSelection)?.index ?? 0)
        
        let value = Language.allCases.map {$0.rawValue}
        alertController.addLanguagePickerView(withValues: [value],
                                              initialSelection: initialSelection) { (vc, picker, index, values) in
                                                let buttonTitle = values[index.column][index.row]
                                                sender.setTitle(buttonTitle, for: .normal)
                                                self.defaults.set(buttonTitle, forKey: sender.restorationIdentifier ?? "")
        }
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { _ in
            guard let searchText = self.searchBar.text, !searchText.isEmpty else {return}
            self.viewModel.getTranslate(searchTerm: searchText)
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true)
    }
    
    @IBAction func termLangugeButtonPressed(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {return}
        self.showLanguageAlertController(sender, title)
    }
    @IBAction func definitionLanguageButtonPressed(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {return}
        self.showLanguageAlertController(sender, title)
    }
    
    @IBAction func replaceLangugeButton(_ sender: UIButton) {
        guard let termLanguage = self.termLanguageButton.titleLabel?.text,
            let definitionLanguage = self.definitionLanguageButton.titleLabel?.text else {return}
        
        self.termLanguageButton.setTitle(definitionLanguage, for: .normal)
        self.definitionLanguageButton.setTitle(termLanguage, for: .normal)
          
        self.defaults.set(termLanguage, forKey: self.definitionLanguageButton.restorationIdentifier ?? "")
        self.defaults.set(definitionLanguage, forKey: self.termLanguageButton.restorationIdentifier ?? "")
    }
    
    //MARK: - Flow functions
    @objc private func dismissKeyboard() {
      self.searchBar.resignFirstResponder()
    }
    
    private func presentCardSetsViewController(with card: TranslateCard) {
        let storyboard = UIStoryboard(name: String(describing: CardSetsListViewContoller.self), bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: String(describing: CardSetsListViewContoller.self)) as? CardSetsListViewContoller else {return}
        controller.modalPresentationStyle = .popover
        controller.sentCardSetComplition = { cardSet in
            self.viewModel.addTranslateCard(card, to: cardSet)
        }
        let navigationController = UINavigationController(rootViewController: controller)
        self.present(navigationController, animated: true)
    }

    func presentMessage() {
        let alert = UIAlertController(title: "Result is empty.", message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }
}

//MARK: - UIPickerViewDelegate
extension SearchBarViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Language.allCases[row].rawValue
    }
}

//MARK: - UIPickerViewDataSource
extension SearchBarViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Language.allCases.count
    }
}

//MARK: - UITableViewDataSource
extension SearchBarViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchBarTableViewCell.self), for: indexPath) as? SearchBarTableViewCell else {return UITableViewCell()}
        let model = self.viewModel.getItem(atIndexPath: indexPath)
        var cellViewModel = SearchBarTableViewCellViewModel(model: model)
        cellViewModel.presentComplitionBlock = { model in
            self.presentCardSetsViewController(with: model)
        }
        cell.configure(with: cellViewModel)

        return cell
    }
}

//MARK: - UISearchBarDelegate
extension SearchBarViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.viewModel.cleanCardResults()
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismissKeyboard()
        guard let searchText = self.searchBar.text, !searchText.isEmpty else {return}
        self.viewModel.getTranslate(searchTerm: searchText)
    }
}


