//
//  TranslateCard.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 13.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct TranslateCard {
    var term: String
    var definition: String
}
