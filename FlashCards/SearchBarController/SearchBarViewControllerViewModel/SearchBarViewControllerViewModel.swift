//
//  SearchBarViewControllerViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

final class SearchBarViewControllerViewModel {
    
    //MARK: - Property
    private var cardResults: [TranslateCard] = []
    private var delegate: SearchBarViewController!
    private var coreDataManager: CoreDataManager<Card>!
    private var termLanguage: Language {
        Language.init(rawValue: UserDefaults.standard.string(forKey: "termLanguage") ?? "") ?? .en}
    private var definitionLanguage: Language {
        Language.init(rawValue: UserDefaults.standard.string(forKey: "definitionLanguage") ?? "") ?? .ru}
    
    //MARK: - Init
    init(delegate: SearchBarViewController) {
        self.delegate = delegate
    }
    
    //MARK: - Flow functions
    
    private func createCoreDataManager(with model: CardSet) {
        let sortDescriptors = [NSSortDescriptor(key: Card.SortKeys.term.rawValue, ascending: true)]
        let predicate = NSPredicate(format: "\(Card.SortKeys.owner.rawValue) == %@", model)
        self.coreDataManager = CoreDataManager<Card>(sortDescriptors: sortDescriptors, predicate: predicate)
    }
    
    func getNumberOfRows() -> Int {
        return self.cardResults.count
    }
    
    func getItem(atIndexPath indexPath: IndexPath) -> TranslateCard {
        return self.cardResults[indexPath.row]
    }
    
    func cleanCardResults() {
        self.cardResults = []
    }
    
    func getTranslate(searchTerm term: String) {
        self.cardResults = []
        
        let termLanguage = String(describing: self.termLanguage.self)
        let definitionLanguage = String(describing: self.definitionLanguage.self)
        
        let key = "dict.1.1.20191207T094906Z.2cbbd002a21d0f92.491f17c0b111df03af4d4e0dac564bd011ac830e"
        let dataLoader = DataLoader()
        let urlString = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup"
        
        var urlComponents = URLComponents(string: urlString)
        urlComponents?.query = "key=\(key)&lang=\(termLanguage)-\(definitionLanguage)&text=\(term.lowercased())"
        
        guard let url = urlComponents?.url else {return}
        
        let decoder = JSONDecoder()
        
        dataLoader.loadData(with: url) { (result) in
            switch result {
            case .data(let data):
                do {
                    let wordsResult = try decoder.decode(YaResponse.self, from: data)
                    let words = wordsResult.def.flatMap {$0.tr}
                        .map{TranslateCard(term: term, definition: $0.text)}
                    self.cardResults = words
                    self.delegate.tableView.reloadData()
                    if self.cardResults.isEmpty {
                        self.delegate.presentMessage()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func addTranslateCard(_ card: TranslateCard, to model: CardSet) {
        self.createCoreDataManager(with: model)
        self.coreDataManager.addNewItem(withTerm: card.term, definition: card.definition, cardDescription: nil, owner: model)
    }
}
