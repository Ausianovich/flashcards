//
//  SearchBarTableViewCellViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

struct SearchBarTableViewCellViewModel{
    
    private var model: TranslateCard
    
    var definition: String
    var presentComplitionBlock: ((TranslateCard)-> Void)?
    
    init(model: TranslateCard) {
        self.model = model
        self.definition = model.definition
    }
    
    func presentCardSetsViewController() {
        guard let presentComplitionBlock = self.presentComplitionBlock else {return}
        presentComplitionBlock(self.model)
    }
}
