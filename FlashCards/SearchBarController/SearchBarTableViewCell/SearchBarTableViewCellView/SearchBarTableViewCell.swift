//
//  SearchBarTableViewCell.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit

final class SearchBarTableViewCell: UITableViewCell {
    
    private var viewModel: SearchBarTableViewCellViewModel?
    
    @IBOutlet var definitionTextLabel: UILabel!
    @IBOutlet var addButton: UIButton!
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        self.viewModel?.presentCardSetsViewController()
    }
    
    func configure(with viewModel: SearchBarTableViewCellViewModel) {
        self.definitionTextLabel.text = viewModel.definition
        self.viewModel = viewModel
    }
    
}
