//
//  NetworkEngine.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

protocol NetworkEngine {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void
    
    func performRequest(for url: URL, complitionHandler: @escaping Handler)
}
