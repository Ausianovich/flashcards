//
//  URLSessionExtension.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

extension URLSession: NetworkEngine {
    typealias Handler = NetworkEngine.Handler
    
    func performRequest(for url: URL, complitionHandler: @escaping Handler) {
        let task = dataTask(with: url, completionHandler: complitionHandler)
        task.resume()
    }
}
