//
//  DataLoader.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

final class DataLoader {
    
    enum Result {
        case data(Data)
        case error(Error)
    }
    
    private let engine: NetworkEngine
    
    init(engine: NetworkEngine = URLSession.shared) {
        self.engine = engine
    }

    func loadData(with url: URL, complitionHandler: @escaping (Result)-> Void) {
        
        self.engine.performRequest(for: url) { (data, response, error) in
            if let error = error {
                complitionHandler(.error(error))
            }
            DispatchQueue.main.async {
                complitionHandler(.data(data ?? Data()))
            }
        }
    }
}
