//
//  YandexResponseModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 23.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

struct YaResponse: Codable {
    let head: Head
    let def: [Def]
}

// MARK: - Def
struct Def: Codable {
    let text: String
    let pos: String?
    let ts: String?
    let tr: [Tr]
}

// MARK: - Tr
struct Tr: Codable {
    let text: String
    let pos: String?
    let gen: String?
    let syn: [Syn]?
    let mean: [Mean]?
    let ex: [Ex]?
    let asp: String?
}

// MARK: - Ex
struct Ex: Codable {
    let text: String
    let tr: [Mean]
}

// MARK: - Mean
struct Mean: Codable {
    let text: String
}

// MARK: - Syn
struct Syn: Codable {
    let text: String
    let pos: String?
    let gen: String?
}

// MARK: - Head
struct Head: Codable {
}
