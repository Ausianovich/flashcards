//
//  LearnCardCollectionViewCell.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 08.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class LearnCardCollectionViewCell: UICollectionViewCell {

    enum CardSide {
        case front, back
    }
    
    private var cardSide: CardSide? {
        didSet {
            guard let cardSide = self.cardSide else {return}
            switch cardSide {
            case .front:
                self.termTextLabel.text = self.term
            case .back:
                self.termTextLabel.text = self.definition
            }
        }
    }
    
    private var term: String?
    private var definition: String?
    
    @IBOutlet var termTextLabel: UILabel!
    
    func configure(with viewModel: LearnCardCollectionViewCellViewModel,to side: CardSide) {
        self.term = viewModel.term
        self.definition = viewModel.definition
        self.cardSide = side
    }
    
    func flipCard() {
        self.cardSide = self.cardSide == .front ? .back : .front
    }
}
