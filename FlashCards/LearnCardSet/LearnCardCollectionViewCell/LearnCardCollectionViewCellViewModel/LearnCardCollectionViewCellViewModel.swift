//
//  LearnCardCollectionViewCellViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 08.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct LearnCardCollectionViewCellViewModel {
    var term: String?
    var definition: String?
    
    init(with model: Card) {
        self.term = model.term
        self.definition = model.definition
    }
}
