//
//  LearnCardSetViewController.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 03.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class LearnCardSetViewController: UIViewController {
    
    //MARK: - Property
    private var viewModel: CardsListViewModel?
    
    //MARK: - @IBOutlets
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let cellNib = UINib(nibName: String(describing: LearnCardCollectionViewCell.self), bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: String(describing: LearnCardCollectionViewCell.self))
    }
    
    //MARK: - @IBActions
    @IBAction func resetBarButtonPressed(_ sender: UIBarButtonItem) {
        self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredVertically, animated: true)
    }
    
    //MARK: - Flow functions
    func configure(with viewModel: CardsListViewModel) {
        self.viewModel = viewModel
    }
}

//MARK: - UICollectionViewDelegate
extension LearnCardSetViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? LearnCardCollectionViewCell else {return}
        UIView.transition(with: cell, duration: 0.4, options: .transitionFlipFromLeft, animations: {
            cell.flipCard()
        }, completion: nil)
    }
}

//MARK: - UICollectionViewDataSource
extension LearnCardSetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.getNumberOfRows(inSection: 0) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LearnCardCollectionViewCell.self), for: indexPath) as? LearnCardCollectionViewCell,
            let viewModel = self.viewModel else {return UICollectionViewCell()}
        let cellModel = viewModel.getItem(at: indexPath)
        let cellViewModel = LearnCardCollectionViewCellViewModel(with: cellModel)
        cell.configure(with: cellViewModel, to: .front)
        cell.layer.cornerRadius = 20
        
        return cell
    }
}
