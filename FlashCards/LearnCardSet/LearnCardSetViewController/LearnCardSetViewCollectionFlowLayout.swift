//
//  LearnCardSetViewCollectionFlowLayout.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 08.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class LearnCardSetViewCollectionFlowLayout: UICollectionViewFlowLayout {
    
    //MARK: - Property
    private let pageWidth: CGFloat = 362
    private let pageHeight: CGFloat = 568
        
    //MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      
        self.scrollDirection = UICollectionView.ScrollDirection.vertical
        self.itemSize = CGSize(width: pageWidth, height: pageHeight)
        self.minimumInteritemSpacing = 10
        
    }
        
    //MARK: - Navigation
    override func prepare() {
        super.prepare()
        self.collectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
        self.collectionView?.contentInset = UIEdgeInsets(top: (collectionView?.bounds.height ?? 0) / 2 - pageHeight / 2,
                                                         left: 0,
                                                         bottom: (collectionView?.bounds.height ?? 0) / 2 - pageHeight / 2,
                                                         right: 0
        )
    }
        
    //MARK: - Flow functions
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let array = super.layoutAttributesForElements(in: rect) else {return []}

        for attributes in array {
            let frame = attributes.frame
            let distance = abs((self.collectionView?.contentOffset.y ?? 0) + (self.collectionView?.contentInset.top ?? 0) - frame.origin.y)
            let scale: CGFloat = 0.7 * min(max((1 -  distance / (collectionView?.bounds.height ?? 0)),0.75), 1)
            attributes.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        return array
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        var newOffset = CGPoint()
        let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        let height = (layout?.itemSize.height ?? 0) + (layout?.minimumLineSpacing ?? 0)
        var offset = proposedContentOffset.y + (collectionView?.contentInset.top ?? 0)

        if velocity.y > 0 {
            offset = height * ceil(offset / height)
        } else if velocity.y == 0 {
            offset = height * round(offset / height)
        } else if velocity.y < 0 {
            offset = height * floor(offset / height)
        }

        newOffset.x = proposedContentOffset.x
        newOffset.y = offset - (collectionView?.contentInset.top ?? 0)

        return newOffset
    }
}
