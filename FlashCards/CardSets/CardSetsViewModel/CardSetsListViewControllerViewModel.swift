//
//  CardSetsListViewControllerViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation
import CoreData

final class CardSetsListViewControllerViewModel {
    
    //MARK: - Property
    private var coreDataManager: CoreDataManager<CardSet>!
    
    //MARK: - Init
    init(delegate: NSFetchedResultsControllerDelegate) {
        let sortDescriptors = [NSSortDescriptor(key: CardSet.SortKeys.title.rawValue, ascending: true)]
        
        self.coreDataManager = CoreDataManager<CardSet>(sortDescriptors: sortDescriptors, predicate: nil)
        self.coreDataManager.fetchResultController.delegate = delegate
    }
    
    //MARK: - Flow functions
    func getNumberOfRows(inSection section: Int) -> Int {
        return self.coreDataManager.getNumberOfRows(inSection: section)
    }
    
    func getItem(at indexPath: IndexPath) -> CardSet {
        return self.coreDataManager.getItem(at: indexPath)
    }
    
    func editItem(at indexPath: IndexPath, to title: String) {
        self.coreDataManager.editItem(at: indexPath, to: title)
    }
    
    func addNewItem(withTitle title: String) {
        self.coreDataManager.addNewItem(withTitle: title)
    }
    
    func removeItem(at indexPath: IndexPath) {
        self.coreDataManager.removeItem(at: indexPath)
    }
    
    func fetchAllItems() {
        self.coreDataManager.fetchAllItems()
    }
}
