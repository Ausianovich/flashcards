//
//  ViewController.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 09.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit
import CoreData

final class CardSetsListViewContoller: UIViewController {
    
    //MARK: - Property
    private var viewModel: CardSetsListViewControllerViewModel!
    var sentCardSetComplition: ((CardSet)->Void)?
    
    //MARK: - @IBOutlet
    @IBOutlet var tableView: UITableView!
    
    //MARK: - Main Funtions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = CardSetsListViewControllerViewModel(delegate: self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let cardSetNib = UINib(nibName: String(describing: CardSetTableViewCell.self), bundle: nil)
        self.tableView.register(cardSetNib, forCellReuseIdentifier: String(describing: CardSetTableViewCell.self))
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
        self.navigationController?.view.backgroundColor = .systemBackground
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.fetchAllItems()
        self.tableView.reloadData()
    }
    
    //MARK: - @IBActions
    @IBAction func addCardSetBarButtonPressed(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Add new set", message: "", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Set name"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self ](action) in
            guard let textField = alertController.textFields?.first,
                let setName = textField.text else {return}
            
            self?.viewModel.addNewItem(withTitle: setName)
            self?.tableView.reloadData()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        present(alertController, animated: true)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? CardsListViewController,
            let cardSet = sender as? CardSet else {return}
        let cardListViewModel = CardsListViewModel(model: cardSet, delegate: controller)
        controller.configure(withViewModel: cardListViewModel)
        
        controller.title = "\(cardSet.title ?? "")"
    }
}

//MARK: - UITableViewDelegate
extension CardSetsListViewContoller: UITableViewDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            self.viewModel.removeItem(at: indexPath)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.presentingViewController != nil {
            let cardSet = self.viewModel.getItem(at: indexPath)
            guard let sentCardSetComplition = self.sentCardSetComplition else { return }
            sentCardSetComplition(cardSet)
            self.dismiss(animated: true, completion: nil)
        } else {
            let cardSet = self.viewModel.getItem(at: indexPath)
            performSegue(withIdentifier: String(describing: CardsListViewController.self), sender: cardSet)
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextualAction = UIContextualAction(style: .normal, title: "Edit") { [weak self](action, view, handler) in
            guard let cell = tableView.cellForRow(at: indexPath) as? CardSetTableViewCell else {return}
            
            let alertController = UIAlertController(title: "Edit set", message: "", preferredStyle: .alert)
            alertController.addTextField { (textField) in
                textField.text = cell.title.text
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "OK", style: .default) { [weak self ](action) in
                guard let textField = alertController.textFields?.first,
                    let setName = textField.text else {return}
                self?.viewModel.editItem(at: indexPath, to: setName)
                self?.tableView.reloadData()
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            self?.present(alertController, animated: true)
            handler(true)
        }
        contextualAction.backgroundColor = .blue
        
        return UISwipeActionsConfiguration(actions: [contextualAction])
    }
}

//MARK: - UITableViewDataSource
extension CardSetsListViewContoller: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CardSetTableViewCell.self), for: indexPath) as? CardSetTableViewCell else {return UITableViewCell()}
        let cellModel = CardSetTableViewCellViewModel(model: self.viewModel.getItem(at: indexPath))
        cell.configure(with: cellModel)
        
        return cell
    }
}

//MARK: - NSFetchedResultsControllerDelegate
extension CardSetsListViewContoller: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .move:
            if let indextPath = indexPath {
                self.tableView.deleteRows(at: [indextPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}

