//
//  CardSetTableViewCellViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

struct CardSetTableViewCellViewModel {
    var title: String?
    var cardCount: Int = 0
    
    init(model: CardSet) {
        self.title = model.title
        self.cardCount = model.cards?.count ?? 0
    }
}
