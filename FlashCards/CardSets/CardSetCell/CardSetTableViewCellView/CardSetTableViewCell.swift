//
//  CardSetTableViewCell.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit

final class CardSetTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var cardsCount: UILabel!
    @IBOutlet var cardBackground: UIView!
    
    func configure(with viewModel: CardSetTableViewCellViewModel) {
        self.title.text = viewModel.title
        self.cardsCount.text = viewModel.cardCount.description
    }
}
