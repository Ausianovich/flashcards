//
//  SettingsTableTableViewController.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 02.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class SettingsTableTableViewController: UITableViewController {
    //MARK: - Property
    
    private var defaults = UserDefaults.standard
    
    //MARK: - IBOutlets
    
    @IBOutlet private var termLanguageButton: UIButton!
    @IBOutlet private var definitionLanguageButton: UIButton!
    
    //MARK: - Main functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.termLanguageButton.setTitle(self.defaults.string(forKey: self.termLanguageButton.restorationIdentifier ?? ""), for: .normal)
        self.definitionLanguageButton.setTitle(self.defaults.string(forKey: self.definitionLanguageButton.restorationIdentifier ?? ""), for: .normal)
    }
    
    //MARK: - IBActions
    
    @IBAction func termLanguageButtonPressed(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {return}
        self.showLanguageAlertController(sender, title)
    }
    
    @IBAction func definitionLanguageButtonPressed(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {return}
        self.showLanguageAlertController(sender, title)
    }
    
    //MARK: - Flow functions
    
    private func showLanguageAlertController(_ sender: UIButton, _ initialSelection: String) {
        let alertController = UIAlertController(title: "Set language", message: nil, preferredStyle: .actionSheet)
        
        let initialSelection: LanguagePickerView.Index = (0, Language.init(rawValue: initialSelection)?.index ?? 0)
        
        let value = Language.allCases.map {$0.rawValue}
        alertController.addLanguagePickerView(withValues: [value],
                                              initialSelection: initialSelection) { (vc, picker, index, values) in
                                                let buttonTitle = values[index.column][index.row]
                                                sender.setTitle(buttonTitle, for: .normal)
                                                self.defaults.set(buttonTitle, forKey: sender.restorationIdentifier ?? "")
        }
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true)
    }
    
}
