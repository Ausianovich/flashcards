//
//  Language.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 21.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

enum Language: String, CaseIterable {
    case ru = "Russian"
    case en = "English"
    case fr = "French"
    case pl = "Polish"
    case uk = "Ukrainian"
    case de = "German"
    case it = "Italian"
    case tr = "Turkish"
}
extension CaseIterable where Self: Equatable {
    var index: Self.AllCases.Index? {
        return Self.allCases.firstIndex { self == $0 }
    }
}
