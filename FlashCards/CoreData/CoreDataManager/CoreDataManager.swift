//
//  CoreDataManager.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 20.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation
import CoreData
import UIKit


final class CoreDataManager<T: NSManagedObject> {
    
    //MARK: - Property
    private var sortDescriptors: [NSSortDescriptor]!
    private var predicate: NSPredicate!
    
    lazy var fetchResultController: NSFetchedResultsController<T> = {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError() }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        fetchRequest.sortDescriptors = self.sortDescriptors
        fetchRequest.predicate = self.predicate
        let fetchedResultsController = NSFetchedResultsController<T>(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }()
    
    //MARK: - Init
    init(sortDescriptors: [NSSortDescriptor], predicate: NSPredicate?) {
        self.sortDescriptors = sortDescriptors
        self.predicate = predicate
    }
    
    //MARK: - Flow functions
    func save () {
        do {
            try self.fetchResultController.managedObjectContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getItem(at indexPath: IndexPath) -> T {
        return self.fetchResultController.object(at: indexPath)
    }
    
    func getNumberOfRows(inSection section: Int) -> Int {
        guard let sections = self.fetchResultController.sections else {return 0}
        return sections[section].numberOfObjects
    }
    
    func removeItem(at indexPath: IndexPath) {
        let item = self.fetchResultController.object(at: indexPath)
        self.fetchResultController.managedObjectContext.delete(item)
        
        self.save()
    }
    
    func fetchAllItems() {
        do {
            try self.fetchResultController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
    }
}

//MARK: - T: CardSet
extension CoreDataManager where T: CardSet {
    func addNewItem(withTitle title: String) {
        let newItem = CardSet(context: self.fetchResultController.managedObjectContext)
        newItem.createCardSet(withTitle: title)
        self.save()
    }
    
    func editItem(at indexPath: IndexPath, to title: String) {
        let item = self.getItem(at: indexPath)
        item.title = title
        self.save()
    }
}

//MARK: - T: Card
extension CoreDataManager where T: Card {
    func addNewItem(withTerm term: String, definition: String, cardDescription: String?, owner: CardSet) {
        let newCard = Card(context: self.fetchResultController.managedObjectContext)
        newCard.configureCard(withTerm: term, definition: definition, cardDescription: cardDescription, owner: owner)
        self.save()
    }
}
