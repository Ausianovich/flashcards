//
//  CardsListViewController.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 22.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit
import CoreData

final class CardsListViewController: UIViewController {
    
    //MARK: - Property
    private var viewModel: CardsListViewModel!

    //MARK: - @IBOutlets
    @IBOutlet var tableView: UITableView!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let cardCellNib = UINib(nibName: String(describing: CardTableViewCell.self), bundle: nil)
        self.tableView.register(cardCellNib, forCellReuseIdentifier: String(describing: CardTableViewCell.self))
        
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 11)
        self.tableView.allowsSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.fetchAllItems()
    }
    
    //MARK: - Navigations
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? LearnCardSetViewController else {return}
        controller.configure(with: self.viewModel)
        controller.title = self.viewModel.getCardSetName()
    }
    
    //MARK: - @IBActions
    @IBAction func addCardBarButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Add new card", message: "", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "card term"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "card definition"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self ](action) in
            guard let termTextField = alertController.textFields?.first,
                let definitionTextField = alertController.textFields?[1],
                let cardTerm = termTextField.text,
                let cardDefinition = definitionTextField.text else {return}
            
            self?.viewModel.addNewCard(withTerm: cardTerm, definition: cardDefinition, cardDescription: nil)
            self?.tableView.reloadData()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func startLearningTabBarButtonPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: String(describing: LearnCardSetViewController.self), sender: nil)
    }
    
    //MARK: - Flow functions
    func configure(withViewModel viewModel: CardsListViewModel) {
        self.viewModel = viewModel
    }
}

//MARK: - UITableViewDelegate
extension CardsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            self.viewModel.removeItem(at: indexPath)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextualAction = UIContextualAction(style: .normal, title: "Edit") { [weak self](action, view, handler ) in
            guard let cell = tableView.cellForRow(at: indexPath) as? CardTableViewCell else {return}
            
            let alertController = UIAlertController(title: "Edit card", message: "", preferredStyle: .alert)
            
            alertController.addTextField { (textField) in
                textField.text = cell.termTextLabel.text
            }
            alertController.addTextField { (textField) in
                textField.text = cell.definitionTextLabel.text
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "OK", style: .default) { [weak self ](action) in
                guard let termTextField = alertController.textFields?.first,
                    let definitionTextField = alertController.textFields?[1],
                    let cardTerm = termTextField.text,
                    let cardDefinition = definitionTextField.text else {return}
                self?.viewModel.removeItem(at: indexPath)
                self?.viewModel.addNewCard(withTerm: cardTerm, definition: cardDefinition, cardDescription: nil)
                self?.tableView.reloadData()
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            self?.present(alertController, animated: true)
            handler(true)
        }
        contextualAction.backgroundColor = .blue
        
        return UISwipeActionsConfiguration(actions: [contextualAction])
    }
}

//MARK: - UITableViewDataSource
extension CardsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CardTableViewCell.self), for: indexPath) as? CardTableViewCell else {return UITableViewCell()}
        let cellModel = CardTableViewCellViewModel(model: self.viewModel.getItem(at: indexPath))
        cell.configure(with: cellModel)
        
        return cell
    }
}

//MARK: - NSFetchedResultsControllerDelegate
extension CardsListViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .move:
            if let indextPath = indexPath {
                self.tableView.deleteRows(at: [indextPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}
