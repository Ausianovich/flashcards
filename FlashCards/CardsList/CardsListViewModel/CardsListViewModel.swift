//
//  CardsListViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 22.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation
import CoreData

final class CardsListViewModel {
    
    //MARK: - Property
    private let model: CardSet
    private let coreDataManager: CoreDataManager<Card>
    
    //MARK: - Init
    init(model: CardSet, delegate: NSFetchedResultsControllerDelegate) {
        self.model = model
        
        let sortDescriptors = [NSSortDescriptor(key: Card.SortKeys.term.rawValue, ascending: true)]
        let predicate = NSPredicate(format: "\(Card.SortKeys.owner.rawValue) == %@", self.model)
        self.coreDataManager = CoreDataManager<Card>(sortDescriptors: sortDescriptors, predicate: predicate)
        self.coreDataManager.fetchResultController.delegate = delegate
    }
    
    //MARK: - Flow functions
    func getNumberOfRows(inSection section: Int) -> Int {
        return self.coreDataManager.getNumberOfRows(inSection: section)
    }
    
    func getItem(at indexPath: IndexPath) -> Card {
        return self.coreDataManager.getItem(at: indexPath)
    }
    
    func addNewCard(withTerm term: String, definition: String, cardDescription: String?) {
        self.coreDataManager.addNewItem(withTerm: term, definition: definition, cardDescription: cardDescription, owner: self.model)
    }
    
    func removeItem(at indexPath: IndexPath) {
        self.coreDataManager.removeItem(at: indexPath)
    }
    
    func fetchAllItems() {
        self.coreDataManager.fetchAllItems()
    }
    
    func getCardSetName() -> String? {
        return self.model.title
    }
}
