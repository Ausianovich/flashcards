//
//  CardTableViewCellViewModel.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 22.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import Foundation

final class CardTableViewCellViewModel {
    var term: String
    var definition: String
    var cardDescription: String?
    
    init(model: Card) {
        self.term = model.term ?? ""
        self.definition = model.definition ?? ""
        self.cardDescription = model.cardDescription
    }
}
