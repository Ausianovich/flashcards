//
//  CardTableViewCell.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 22.12.2019.
//  Copyright © 2019 Constantine inc. All rights reserved.
//

import UIKit

final class CardTableViewCell: UITableViewCell {

    @IBOutlet var termTextLabel: UILabel!
    @IBOutlet var definitionTextLabel: UILabel!
    
    func configure(with model: CardTableViewCellViewModel) {
        self.termTextLabel.text = model.term
        self.definitionTextLabel.text = model.definition
    }
}
