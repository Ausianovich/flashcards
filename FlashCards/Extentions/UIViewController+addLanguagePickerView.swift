//
//  UIViewController+addLanguagePickerView.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 13.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

extension UIAlertController {
    func addLanguagePickerView(withValues values: LanguagePickerView.Values,
                               initialSelection: LanguagePickerView.Index? = nil,
                               action: LanguagePickerView.Action?) {
        let pickerView = LanguagePickerView(withValues: values,
                                            initialSelection: initialSelection,
                                            action: action)
        self.set(vc: pickerView, height: 120)
    }
    
    func set(vc: UIViewController?, width: CGFloat? = nil, height: CGFloat? = nil) {
        guard let vc = vc else { return }
        setValue(vc, forKey: "contentViewController")
        if let height = height {
            vc.preferredContentSize.height = height
            preferredContentSize.height = height
        }
    }
}
