//
//  UserDefaults+isFirstLaunch.swift
//  FlashCards
//
//  Created by Ausianovich Kanstantsin on 02.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

extension UserDefaults {
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        _ = UserDefaults.standard.string(forKey: "termLanguage")
        _ = UserDefaults.standard.string(forKey: "definitionLanguage")
        
        if isFirstLaunch {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.set("English", forKey: "termLanguage")
            UserDefaults.standard.set("Russian", forKey: "definitionLanguage")
        }
        return isFirstLaunch
    }
}
